#!/usr/bin/env python3

import unittest

from pita.utils import Log
from pita.walker import Pita
from pita.walker.debugassert import PitaException
from pita.walker import walker_log


class Errors(unittest.TestCase):

    ingredients = {
        "ingredients": [
            {
                "weight": 0.1,  # kg
                "price_per_kg": 10,  # $/kg
                "name": "chocolate",
            },
            {
                "weight": 0.5,  # kg
                "price_per_kg": 1,  # $/kg
                "name": "floor",
            },
        ],
    }

    stub_value = {
        "rank": 0,
        "stub": {
            "size": 10,
            "weight": 0,
            "toppings": ["marmelade", "caramel", "coconut"],
            "accumulated-weight": 10,
        },
        "layers": [
            {},
            {},
        ],
    }

    def setUp(self):
        walker_log.verbose_level = Log.DOT
        self.p = Pita()
        self.p.root.set_value(Errors.ingredients)
        self.p.root.set_value(Errors.stub_value, hide=True)

    def tearDown(self):
        pass

    def test_fout_split(self):
        @self.p.add_job(
            "/",
            "stub/size",
            "/ingredients/*/size",
        )
        def compute_size(size):
            return size

        with self.assertRaises(PitaException):
            self.p.run()

    def test_ref_and_following(self):
        @self.p.add_job(
            "/ingredients/*",
            "&/weight",
            "/weight",
        )
        def compute_after_ref(weight):
            return weight

        with self.assertRaises(PitaException):
            self.p.run()

    def test_basepath_deadend(self):
        @self.p.add_job("/ingredients/*/flavor", "e", "e-plus")
        def compute_basepath_deadend(e):
            return e

        s = self.p.run()
        self.assertEqual(s, Pita.UNSOLVED)


# FFS
# https://medium.com/@vladbezden/using-python-unittest-in-ipython-or-jupyter-732448724e31
unittest.main(argv=["first-arg-is-ignored"], exit=False)
