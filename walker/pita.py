#!/usr/bin/env python3

import pprint

from pita.utils import log_action, indent_log, json_str

from .job import Job
from .basepath import BasePath
from .functionio import FunctionIn, FunctionOut
from .node import Node
from .treewalker import TravelStatus
from .dot_output import Dot
from .debugassert import dbg_assert
from .walkerlog import log


class Pita:
    """
    Entry point for the computation engine, the assert configuration and the
    dot output configuration.

    A user typically add data then functions and retrieves the result.
    """

    OK = 0
    UNSOLVED = 1
    CYCLE = 2

    def __init__(
        self, data=None, auto_hide=True, except_dump="except", **kwargs
    ):
        self.auto_hide = auto_hide
        self.jobs = []
        # this list is used as a set but with predictable pop() order
        self.pending = []
        self.root = Node(None, "/", False)
        self.except_dump = except_dump
        self.exit = False
        if data:
            self.add_data(data, **kwargs)

    def add_data(self, data, **kwargs):
        self.root.set_value(
            data, hide=self.auto_hide or kwargs.get("hide", False)
        )

    @indent_log
    def add_job(
        self,
        base_path_init,
        input_init,
        output_init,
        *,
        hide=False,
        follow=None,
    ):
        @indent_log
        def inner(fct):
            if follow is not None:
                log.set_tmp_level(follow)
            j = Job.from_init(self, self.root, fct, hide, follow)
            base_path = BasePath.from_init(
                j, self.root, base_path_init, follow
            )
            inputs = FunctionIn.from_init(j, self.root, input_init, follow)
            outputs = FunctionOut.from_init(j, self.root, output_init, follow)
            j.set_paths(base_path, inputs, outputs)
            self.jobs.append(j)
            j.print_indented(log.dbg, pre="add job")
            if j.follow is not None:
                log.restore_level()

        return inner

    def process_job_iter(self, j):
        ok = True
        if j.follow is not None:
            log.set_tmp_level(j.follow)
        log_action(log.dbg, "next pending job", j)
        s, new_jobs = j.travel()
        log_action(log.dbg, "job ret:", TravelStatus.to_str(s))
        if s == TravelStatus.SPLIT:
            self.jobs.remove(j)
            self.jobs += new_jobs
            # do not add new jobs to pending as they will do it
            # themselves
        if s == TravelStatus.GOAL:
            self.jobs.remove(j)
            try:
                j.compute()
            except Exception as e:
                if j.follow is not None:
                    log.restore_level()
                if self.except_dump:
                    self.pprint_root(log.error)
                    self.write_dot(self.except_dump)
                raise e
            j.print_indented(log.dbg, pre="job done")
        if s == TravelStatus.DEADEND:
            log_action(log.vdbg, "deadend")
            ok = False
        if j.follow is not None:
            log.restore_level()
        return ok

    @indent_log
    def run(self):
        self.root.print_indented(log.vdbg, pre="run on data:")
        self.exit = False
        while True:
            if not self.jobs:
                log_action(log.vvdbg, "solved")
                return Pita.OK
            if not self.pending:
                log_action(log.dbg, "unsolved, no pending job")
                return Pita.UNSOLVED
            while self.pending:
                if self.exit:
                    return Pita.UNSOLVED
                j = self.pending.pop(0)
                ok = self.process_job_iter(j)
                if not ok:
                    return Pita.UNSOLVED

    def update(self, job):
        if job not in self.pending:
            log_action(log.vvdbg, "add pending job", job)
            self.pending.append(job)
        else:
            log_action(
                log.vvdbg, "not adding pending job (already pending)", job
            )

    def error(self, msg):
        self.exit = True
        log.error(msg)

    def dotter(self):
        return Dot(self.root, self.jobs)

    def set_assert_output(self, *, show_tree=False, show_dot=False):
        """enbale/disable:
        - tree output on stdout
        - dot output (show_dot must be an open file)
        """
        dbg_assert.tree = self.root if show_tree else None
        dbg_assert.dotter = self.dotter(show_dot) if show_dot else None

    def status_str(self, status):
        strs = {
            Pita.OK: "OK",
            Pita.UNSOLVED: "unsolved",
            Pita.CYCLE: "cycle",
        }
        return strs[status] if status in strs else "unknown"

    def pprint_root(self, log_level, *args, **kwargs):
        pp = pprint.PrettyPrinter(indent=4)
        log_level(pp.pformat(self.root.gather()), *args, **kwargs)

    def json_dump_root(self, log_level, *args, **kwargs):
        log_level(json_str(self.root.gather()), *args, **kwargs)

    def print_stalled_jobs(self, log_level):
        for i in sorted(self.jobs, key=lambda x: -x.heuristic()):
            log_action(log_level, i.one_liner())

    def write_dot(self, filebase):
        with open(f"{filebase}.gv", "w+") as f:
            self.dotter().print_graph(f)
