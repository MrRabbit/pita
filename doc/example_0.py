#!/usr/bin/env python

from math import pi

from pita.walker import Pita, walker_log

# Provide stub data
stub = {
    "circles": [
        {"radius": 4},
        {"radius": 6},
    ]
}


# p will be used to store data, register functions and build output
p = Pita(stub, auto_hide=False)


# Add a function computing new data from existing data
@p.add_job("/circles/*", "radius", "area")
def circle_area(radius):
    return pi * radius**2


# process and write result
status = p.run()

with open("example_0.json", "w+") as f:
    p.json_dump_root(walker_log.dot, file=f)
