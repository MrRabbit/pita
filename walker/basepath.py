#!/usr/bin/env python3

from pita.utils import print_start, signature, indent_log, log_action
from .treewalker import TravelStatus, TreeWalker
from .walkerlog import log


class BasePath:
    """
    a base path targets a node using a chain of routes.
    If the first route is a dead end, it tries the next one.
    When the current route splits, N new base paths are created: their 1st
    route if the result of the split, other untested routes are copied
    """

    def __init__(self, watcher, walkers, follow):
        self.uniq = BasePath.get_id()
        self.watcher = watcher
        self.walkers = walkers
        self.destination = None
        self.follow = follow
        for i in self.walkers:
            i.watcher = self

    def unlink(self):
        for i in self.walkers:
            i.unlink()

    @classmethod
    def from_init(cls, watcher, root, paths, follow):
        if isinstance(paths, str):
            paths = [paths]
        return BasePath(
            watcher,
            [
                TreeWalker.from_init(watcher, root, root, i, follow)
                for i in paths
            ],
            follow
        )

    def clone(self):
        # cloning walkers has already been performed in the split procedure
        return BasePath(None, self.walkers, self.follow)

    @indent_log
    def travel(self):
        if not self.walkers:
            return TravelStatus.DEADEND, []
        tw = self.walkers[0]
        s, new_walkers = tw.travel()
        if s == TravelStatus.SPLIT:
            self.next_tw()
            return TravelStatus.SPLIT, [
                BasePath(self.watcher, [i] + self.walkers, self.follow)
                for i in new_walkers
            ]
        elif s == TravelStatus.GOAL:
            self.destination = tw.node
        elif s == TravelStatus.DEADEND:
            self.next_tw()
            return self.travel()
        return s, []

    def update(self, walker):
        log_action(log.vvdbg, "update", self, walker)
        self.watcher.update(self)

    def head(self):
        return self.walkers[0].node

    def next_tw(self):
        tw = self.walkers.pop(0)
        tw.unlink()
        log_action(log.vvdbg, "basepath pop ", self, tw)

    def heuristic(self):
        return self.walkers[0].heuristic() if self.walkers else 0

    # print functions

    uniq_id = 0

    @classmethod
    def get_id(cls):
        cls.uniq_id += 1
        return cls.uniq_id

    def signature(self):
        return f"BasePath {self.uniq}"

    def print_indented(self, log_level, indent=0, *, pre=""):
        print_start(
            log_level,
            self,
            indent,
            pre,
            body=(
                f"-> {signature(self.destination)} "
                + f"> {signature(self.watcher)}"
            ),
        )
        for i in self.walkers:
            i.print_indented(log_level, indent + 1)

    def one_liner(self):
        return self.walkers[0].one_liner() if self.walkers else ""

    def __str__(self):
        return self.signature()

    def __repr__(self):
        return self.__str__()
