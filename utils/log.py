#!/usr/bin/env python3

from sys import stdout, stderr, exc_info
import io
import functools
import _testcapi

try:
    import highlightcli

    def highlight(*args, **kwargs):
        return highlightcli.highlight(*args, **kwargs)

except ImportError:
    def highlight(string, *args, **kwargs):
        return string


class Log:
    NONE = -1  # no output at all
    DOT = 0  # write dot output
    TEST = 1  # show currently debugging traces (only for temporary debug)
    ERROR = 2  # fatal error, it should stop
    WARN = 3  # non fatal, it may continue but there might be a catch
    INFO = 4  # non error but important message
    DBG = 5  # only for debug, coarse trace
    VDBG = 6  # only for debug, fine trace
    VVDBG = 7  # only for debug, complete trace
    ACTION = 8  # action prefix marker style

    themes = {
        "dark": {  # fits quite well on a dark terminal background
            DOT: None,
            TEST: "Cyan",
            ERROR: "Red",
            WARN: "Yellow",
            INFO: "Light yellow",
            DBG: "Light blue",
            VDBG: "Light gray",
            VVDBG: "Dark gray",
            ACTION: "Green",
        },
        "none": {  # disable styling
            DOT: None,
            TEST: None,
            ERROR: None,
            WARN: None,
            INFO: None,
            DBG: None,
            VDBG: None,
            VVDBG: None,
            ACTION: None,
        },
    }

    # adapt the theme by setting this value
    th = themes["dark"]

    tmp_levels = []

    end_marker = highlight("§ ", th[ACTION])

    def __init__(self, verbose_level):
        self.verbose_level = verbose_level

    def log(self, level, *args, file=stdout, **kwargs):
        if self.verbose_level >= level:
            buf = io.StringIO()
            print(*args, **kwargs, file=buf)
            raw_str = buf.getvalue()
            buf.close()
            print(highlight(raw_str, Log.th[level]), end="", file=file)

    def dot(self, *args, **kwargs):
        self.log(Log.DOT, *args, **kwargs)

    def test(self, *args, **kwargs):
        self.log(Log.TEST, *args, file=stderr, **kwargs)

    def error(self, *args, file=stderr, **kwargs):
        self.log(Log.ERROR, *args, file=file, **kwargs)

    def warn(self, *args, **kwargs):
        self.log(Log.WARN, *args, **kwargs)

    def info(self, *args, **kwargs):
        self.log(Log.INFO, *args, **kwargs)

    def dbg(self, *args, **kwargs):
        self.log(Log.DBG, *args, **kwargs)

    def vdbg(self, *args, **kwargs):
        self.log(Log.VDBG, *args, **kwargs)

    def vvdbg(self, *args, **kwargs):
        self.log(Log.VVDBG, *args, **kwargs)

    def set_tmp_level(self, level):
        self.tmp_levels.append(self.verbose_level)
        self.verbose_level = level

    def restore_level(self):
        if self.tmp_levels:
            self.verbose_level = self.tmp_levels.pop(-1)


def is_json_serializable(obj):
    return any(
        [
            "to_json" in i.__dict__
            for i in [type(obj)] + list(type(obj).__bases__)
        ]
    )


def json_str(data, indent_spaces=2, pre="", indent=0, last=True):
    suffix = "" if last else ","
    ret = " " * indent_spaces * indent + str(pre)
    if isinstance(data, dict):
        ret += "{\n"
        for idx, (k, v) in enumerate(data.items()):
            ret += json_str(
                v,
                indent_spaces,
                pre=f'"{k}": ',
                indent=indent + 1,
                last=idx == len(data) - 1,
            )
        ret += " " * indent_spaces * indent + "}" + suffix + "\n"
    elif isinstance(data, list):
        ret += "[" + "\n"
        for idx, i in enumerate(data):
            ret += json_str(
                i,
                indent_spaces,
                pre="",
                indent=indent + 1,
                last=idx == len(data) - 1,
            )
        ret += " " * indent_spaces * indent + "]" + suffix + "\n"
    elif isinstance(data, bool):
        ret += f"{'true' if data else 'false'}{suffix}\n"
    else:
        if isinstance(data, str):
            data = f'"{data}"'
        elif is_json_serializable(data):
            data = f'"{str(data)}"'
        else:
            data = str(data)
        ret += data + suffix + "\n"
    return ret


action_indent = 0


def indent_log(fct):
    """decorator to recursively indent log outputs"""

    # remove itself from bactrace:
    # https://stackoverflow.com/questions/72146438/remove-decorator-from-stack-trace
    @functools.wraps(fct)
    def inner(*args, **kwargs):
        try:
            global action_indent
            action_indent += 1
            ret = fct(*args, **kwargs)
            action_indent -= 1
            return ret
        except Exception:
            tp, exc, tb = exc_info()
            _testcapi.set_exc_info(tp, exc, tb.tb_next)
            del tp, exc, tb
            raise

    return inner


def log_action(log_level, *args, **kwargs):
    """
    log an action that is characterized by a '§' prefix, as opposed to a
    structure output
    """
    log_level(" " * 2 * action_indent, end=Log.end_marker)
    log_level(*args, **kwargs)


def print_start(log_level, obj, indent, pre="", body=""):
    """to be called when starting to print a pita structure"""
    log_level(" " * (indent + action_indent) * 2, end="")
    if pre:
        log_level(pre + " :", end=" ")
    log_level(obj.signature(), end="")
    log_level(body)


def signature(obj):
    if obj:
        return obj.signature()
    return "None"
