#!/usr/bin/env python

from math import pi

from pita.walker import Pita, walker_log


stub = {
    "plate-radius": 10,
    "first-height": 5,
    "decrease-factor": 0.8,
    "min-radius": 4,
    # add the candle weight with the same name as the layers accumulated weight
    "accumulated-weight": 4,
    # and the cake density to compute the weight from the volume
    "cake-density": 1,
}

p = Pita({"stub": stub})


@p.add_job("/layers/*", ["area", "height"], "volume")
def cylinder_volume(area, height):
    return area * height


@p.add_job("/layers/*", "radius", "area")
def circle_area(radius):
    return pi * radius**2


@p.add_job(
    "/stub",
    ["plate-radius", "first-height", "decrease-factor", "min-radius"],
    "/layers",
)
def circles(radius, height, decrease_factor, min_radius):
    layers = []
    while radius >= min_radius:
        layers.append({"radius": round(radius, 1), "height": round(height, 1)})
        radius *= decrease_factor
        height *= decrease_factor
    return layers


@p.add_job("/layers/*", ["#", "!"], "has-candle")
def candle(count, index):
    return index == count - 1


# compute each layer own weight
@p.add_job("/layers/*", ["volume", "/stub/cake-density"], "weight")
def compute_own_weight(volume, density):
    return volume * density


# /stub is a fallback when /layers/N+1 does not exist
# in this case /layers/+/accumulated-weight is replaced with
# /stub/accumulated-weight
@p.add_job(
    ["/layers/*", "/stub"],
    ["weight", "+/accumulated-weight"],
    "accumulated-weight",
)
def compute_accumulated_weight(own_weight, next_accumulated_weight):
    return own_weight + next_accumulated_weight


status = p.run()

with open("example_4.json", "w+") as f:
    p.json_dump_root(walker_log.dot, file=f)
