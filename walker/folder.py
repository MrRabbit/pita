#!/usr/bin/env python3

from .walkerlog import log


class Folder:
    """
    Describes a single semantic node in the data tree
    """

    GENERIC = 1
    ROOT = 2
    PARENT = 3
    WILDCARD = 4
    PREVIOUS = 5
    NEXT = 6
    COUNT = 7
    INDEX = 8
    REFERENCE = 9  # not tested, may be removed

    def __init__(self, name, folder_type):
        try:
            self.name = int(name)
        except ValueError:
            self.name = name
            if not name:
                log.warn("empty folder name")
        self.folder_type = folder_type

    @classmethod
    def from_str(cls, path):
        ret = []
        parts = path.split("/")
        # if path starts with a /, the first element is empty
        if parts and not parts[0]:
            ret.append(Folder("/", Folder.ROOT))
            parts.pop(0)
        for i in parts:
            if not i:
                continue
            if i == "..":
                ret.append(Folder(i, Folder.PARENT))
            elif i == "*":
                ret.append(Folder(i, Folder.WILDCARD))
            elif i == "-":
                ret.append(Folder(i, Folder.PREVIOUS))
            elif i == "+":
                ret.append(Folder(i, Folder.NEXT))
            elif i == "#":
                ret.append(Folder(i, Folder.COUNT))
            elif i == "!":
                ret.append(Folder(i, Folder.INDEX))
            elif i == "&":
                ret.append(Folder(i, Folder.REFERENCE))
            else:
                ret.append(Folder(i, Folder.GENERIC))
        return ret

    @classmethod
    def readlink(cls, folders):
        """ show full path, as the UNIX function readlink"""
        if not folders:
            return ""
        ret = '/'.join([str(i.name) for i in folders])
        if not ret:
            return ""
        if ret[0] == "/":
            ret = ret[1:]
        return ret

    def type_str(self):
        if self.folder_type == Folder.GENERIC:
            return "generic"
        elif self.folder_type == Folder.ROOT:
            return "root"
        elif self.folder_type == Folder.PARENT:
            return "parent"
        elif self.folder_type == Folder.WILDCARD:
            return "wildcard"
        elif self.folder_type == Folder.PREVIOUS:
            return "previous"
        elif self.folder_type == Folder.NEXT:
            return "next"
        elif self.folder_type == Folder.COUNT:
            return "count"
        elif self.folder_type == Folder.INDEX:
            return "index"

    # print functions
    def signature(self):
        return (
            f"F<{self.folder_type}>{self.name}"
        )

    def __str__(self):
        return self.signature()

    def __repr__(self):
        return self.__str__()
