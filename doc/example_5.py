#!/usr/bin/env python

from math import pi

from pita.walker import Pita, walker_log


stub = {
    "plate-radius": 10,
    "first-height": 5,
    "decrease-factor": 0.8,
    "min-radius": 4,
    "accumulated-weight": 4,
    "cake-density": 1,
}

p = Pita({"stub": stub})


@p.add_job("/layers/*", ["area", "height"], "volume")
def cylinder_volume(area, height):
    return area * height


@p.add_job("/layers/*", "radius", "area")
def circle_area(radius):
    return pi * radius**2


@p.add_job(
    "/stub",
    ["plate-radius", "first-height", "decrease-factor", "min-radius"],
    "/layers",
)
def circles(radius, height, decrease_factor, min_radius):
    layers = []
    while radius >= min_radius:
        layers.append({"radius": round(radius, 1), "height": round(height, 1)})
        radius *= decrease_factor
        height *= decrease_factor
    return layers


# compute the perimeter of each layer to feed the glacage() function
@p.add_job("/layers/*", "radius", "perimeter")
def perimeter(radius):
    return radius * pi


# take a list of all perimeters and a list of all heights
@p.add_job("/layers", ["*/perimeter", "*/height"], "/glacage")
def glacage(perimeter, height):
    return sum([p*h for p, h in zip(perimeter, height)])


status = p.run()

with open("example_5.json", "w+") as f:
    p.json_dump_root(walker_log.dot, file=f)
