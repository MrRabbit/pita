#!/usr/bin/env python3

from .walkerlog import log


class PitaException(Exception):
    pass


class DbgAssert:
    """
    similar to assert but prints the data tree and the dot graph if requested
    """

    def __init__(self):
        self.tree = None
        self.dotter = None

    def check(self, cond, msg):
        if cond:
            return
        log.error(msg)
        if self.tree:
            self.tree.print_indented(log.info, pre="tree")
            if self.dotter:
                self.dotter.print_graph()
        raise PitaException


dbg_assert = DbgAssert()
