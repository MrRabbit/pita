from sys import stderr

from .walkerlog import log
from .node import Node
from .debugassert import dbg_assert


class Dot:
    """
    output the graph as a Graphviz dot file.

    file_ is an open file

    mode may be:
    - "nodes": only print nodes
    - "with-stalled-jobs": print nodes and unsolved jobs with their links to
    the pending nodes
    """

    def __init__(self, root, jobs):
        self.root = root
        self.jobs = jobs
        self.color = {
            "node-link": '"#000000"',
            "job-dep": '"#0000AA"',
        }

    def print_preamble(self):
        log.dot("digraph poc {", file=self.cur_file)
        # buggy for now: https://gitlab.com/graphviz/graphviz/-/issues/2283
        # log.dot("  beautify=true", file=self.cur_file)
        # maybe add this as an option
        # log.dot("  layout=\"sfdp\"", file=self.cur_file)
        log.dot('  graph [fontname = "helvetica"];', file=self.cur_file)
        log.dot('  node [fontname = "helvetica"];', file=self.cur_file)
        log.dot('  edge [fontname = "helvetica"];', file=self.cur_file)

    def print_end(self):
        log.dot("}", file=self.cur_file)

    def add_edge(self, a, b, color, *, style="solid"):
        if a and b:
            log.dot(
                f'  "{a.signature()}" -> "{b.signature()}" '
                f"[style={style}, color={color}]",
                file=self.cur_file,
            )

    def add_job(self, j):
        log.dot(
            f'  "{j.signature()}" [shape=box,'
            f" style={'dotted' if j.done else 'solid'}]",
            file=self.cur_file,
        )

    def add_node_links(self, n):
        if n.parent:
            self.add_edge(n.parent, n, self.color["node-link"])

    def print_jobs(self):
        for j in self.jobs:
            for f in j.pending:
                for t in f.pending:
                    self.add_job(j)
                    self.add_edge(t.node, j, self.color["job-dep"])

    def print_graph(self, file_=stderr, mode="with-stalled-jobs"):
        dbg_assert.check(
            mode in ["nodes", "with-stalled-jobs"], "wrong graph mode"
        )
        self.cur_file = file_ if file_ else self.file_
        cur_mode = mode if mode else self.mode

        self.print_preamble()
        if self.root:
            self.root.travel(Node.PREFIX, lambda n: self.add_node_links(n))
        if cur_mode == "with-stalled-jobs":
            self.print_jobs()
        self.print_end()
