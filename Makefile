
all: coverage

tests:
	python -m coverage run  --omit=test* -m unittest

coverage: tests
	python -m coverage report
	python -m coverage html
