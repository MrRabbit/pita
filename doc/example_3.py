#!/usr/bin/env python

from math import pi

from pita.walker import Pita, walker_log

# Compared to the previous example, Add the height
stub = {
    # the first layer radius is the plate radius
    "plate-radius": 10,
    # the first layer height
    "first-height": 5,
    # each layer radius and height is smaller than the previous one by this
    # factor
    "decrease-factor": 0.8,
    # iteratively decrease layers dimensions, until this limit is reached
    "min-radius": 4,
}

# put the stub in the "stub" child node to separate it from the result
p = Pita({"stub": stub})


@p.add_job("/layers/*", ["area", "height"], "volume")
def cylinder_volume(area, height):
    return area * height


# keep the old functions, working on the computed radii and heights
@p.add_job("/layers/*", "radius", "area")
def circle_area(radius):
    return pi * radius**2


# iteratively compute radii and heights instead of specifiying it in the stub
@p.add_job(
    "/stub",
    ["plate-radius", "first-height", "decrease-factor", "min-radius"],
    "/layers",
)
def circles(radius, height, decrease_factor, min_radius):
    layers = []
    while radius >= min_radius:
        layers.append({"radius": round(radius, 1), "height": round(height, 1)})
        radius *= decrease_factor
        height *= decrease_factor
    return layers


@p.add_job("/layers/*", ["#", "!"], "has-candle")
def candle(count, index):
    return index == count - 1


status = p.run()

with open("example_3.json", "w+") as f:
    p.json_dump_root(walker_log.dot, file=f)
