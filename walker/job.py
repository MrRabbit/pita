#!/usr/bin/env python3

from collections.abc import Iterable

from pita.utils import print_start, indent_log, log_action, highlight
from .treewalker import TravelStatus
from .walkerlog import log


class Job:
    """
    A job takes semantic paths of data and tries to resolve them to tree
    nodes. On success, It computes the user function, build the new nodes and
    tells other jobs that these new nodes are available to be used as inputs.

    The paths are divided into a prefix: the base path and a suffix: the I/Os.
    The base path is common for all IOs.If a job splits while parsing the base
    path, it splits itself into several new jobs. If the job splits while
    parsing a function input, it keeps this as a single input which contains a
    list of data.
    """

    @indent_log
    def __init__(
        self,
        watcher,
        abs_root,
        fct,
        basepath,
        fin,
        fout,
        waiting_basepath,
        hide,
        follow,
    ):
        self.uniq = Job.get_id()
        self.watcher = watcher
        self.abs_root = abs_root
        self.fct = fct
        self.basepath = basepath
        self.fin = fin
        self.fout = fout
        self.pending = self.fin + self.fout
        self.waiting_basepath = waiting_basepath
        self.hide = hide
        self.follow = follow
        self.ready = False
        if self.basepath:
            self.basepath.watcher = self
        for i in self.fin:
            i.watcher = self
        for i in self.fout:
            i.watcher = self
        self.watcher.update(self)
        self.done = False

    def unlink(self):
        self.basepath.unlink()
        for i in self.pending:
            i.unlink()

    @classmethod
    def from_init(cls, watcher, abs_root, fct, hide, follow):
        return Job(watcher, abs_root, fct, None, [], [], [], hide, follow)

    @indent_log
    def set_paths(self, base_path, inputs, outputs):
        self.basepath = base_path
        self.fin = inputs
        self.fout = outputs
        self.pending = self.fin + self.fout
        self.waiting_basepath = list(self.pending)
        if self.basepath:
            self.basepath.watcher = self
        for i in self.fin:
            i.watcher = self
        for i in self.fout:
            i.watcher = self

    @indent_log
    def fork(self, basepath):
        fin = []
        fout = []
        waiting_bb = []
        for i in self.fin:
            n = i.fork(basepath.head())
            fin.append(n)
            if i in self.waiting_basepath:
                waiting_bb.append(n)
        for i in self.fout:
            n = i.fork(basepath.head())
            fout.append(n)
            if i in self.waiting_basepath:
                waiting_bb.append(n)
        return Job(
            self.watcher,
            self.abs_root,
            self.fct,
            basepath.clone(),
            fin,
            fout,
            waiting_bb,
            self.hide,
            self.follow,
        )

    @indent_log
    def travel(self):
        if self.done:
            log.warn("job already done: ", self)
            return TravelStatus.GOAL, []
        if self.waiting_basepath:
            return self.travel_basepath()
        return self.travel_args(), []

    @indent_log
    def travel_basepath(self):
        log_action(log.vdbg, "travel basepath")
        s, replace_bp = self.basepath.travel()
        self.basepath.print_indented(
            log.vdbg,
            pre="job basepath travel: "
            + highlight(f"{TravelStatus.to_str(s)}", bold=True),
            indent=1,
        )
        replace_job = []
        if s == TravelStatus.SPLIT:
            self.done = True  # done because it won't be computed
            replace_job = [Job.fork(self, i) for i in replace_bp]
            for i in replace_job:
                i.print_indented(log.vdbg, pre="created job", indent=1)
            self.unlink()
        elif s == TravelStatus.GOAL:
            for i in self.waiting_basepath:
                i.set_rel_root(self.basepath.destination)
            self.waiting_basepath.clear()
            return self.travel_args(), []
        return s, replace_job

    def travel_args(self):
        # postpone the basepath pop(): if multiple args are in the
        # DEADEND_RETRY status, this would erroneously pop several tw
        next_bp = False
        log_action(log.vdbg, "travel args")
        ret = TravelStatus(TravelStatus.STALL)
        for i in list(self.pending):  # iterate on a copy to allow erase
            log_action(log.vdbg, "next arg", i)
            if i in self.waiting_basepath:
                continue
            s = i.travel()
            i.print_indented(
                log.vdbg,
                pre="job arg travel "
                + highlight(f"{TravelStatus.to_str(s)}", bold=True),
                indent=1,
            )
            if s == TravelStatus.GOAL:
                self.pending.remove(i)
                ret.set_max(TravelStatus.MOVE)
            elif s == TravelStatus.SPLIT:
                ret.set_max(TravelStatus.MOVE)
            elif s == TravelStatus.DEADEND:
                return TravelStatus(TravelStatus.DEADEND)
            elif s == TravelStatus.DEADEND_RETRY:
                next_bp = True
                self.waiting_basepath.append(i)
                ret.set_max(TravelStatus.MOVE)
                self.watcher.update(self)
            else:
                ret.set_max(s)
        if next_bp:
            self.basepath.next_tw()
        if not self.pending:
            self.ready = True
            return TravelStatus.GOAL
        return ret

    @indent_log
    def update(self, watchee):
        log_action(
            log.vvdbg,
            "update",
            self,
            watchee in self.fin or watchee in self.fout,
        )
        if watchee in self.fin or watchee in self.fout:
            self.pending.append(watchee)
        self.watcher.update(self)

    def error(self, msg):
        self.watcher.error(msg)

    @indent_log
    def compute(self):
        self.done = True
        args = [i.value() for i in self.fin]
        log_action(log.vdbg, f"running {self.fct.__name__} with {args}")
        out = self.fct(*args)
        n_produced = len(out) if isinstance(out, Iterable) else 1
        n_expected = len(self.fout)
        if n_expected == 1:
            log.vdbg(f"set node value single: {self.fout[0]}, {out}")
            self.fout[0].set_value(out, hide=self.hide)
        elif n_produced == n_expected:
            for v, n in zip(out, self.fout):
                log.vdbg(f"set node value list: {n}, {v}")
                n.set_value(v, hide=self.hide)

    def heuristic(self):
        return (
            self.basepath.heuristic()
            + sum([i.heuristic() for i in self.fin])
            + sum([i.heuristic() for i in self.fout])
        )

    # print functions
    uniq_id = 0

    @classmethod
    def get_id(cls):
        cls.uniq_id += 1
        return cls.uniq_id

    def signature(self):
        return f"Job {self.uniq} ({self.fct.__name__})"

    def print_indented(self, log_level, indent=0, *, pre=""):
        print_start(log_level, self, indent, pre)
        self.basepath.print_indented(log_level, indent + 1)
        for i in self.fin:
            i.print_indented(
                log_level,
                indent + 1,
                pre="< *" if i in self.pending else "<  ",
            )
        for i in self.fout:
            i.print_indented(
                log_level,
                indent + 1,
                pre="> *" if i in self.pending else ">  ",
            )

    def one_liner(self):
        return (
            f"{self.signature()} [{self.heuristic()}]:"
            f" {self.basepath.one_liner()} <<<"
            f" {' < '.join([i.one_liner() for i in self.fin])} >>>"
            f" {' > '.join([i.one_liner() for i in self.fout])}"
        )

    def __str__(self):
        return self.signature()

    def __repr__(self):
        return self.__str__()
