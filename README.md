# Pita

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

![pipeline](https://gitlab.com/MrRabbit/pita/badges/main/pipeline.svg)
![coverage](https://gitlab.com/MrRabbit/pita/badges/main/coverage.svg?job=coverage)

Build data from data and functions.

This helps to write small functions with clearly identified inputs and outputs
instead of writing spaghetti code.

[[_TOC_]]

## Installation

TODO

## Introduction

Here is a basic usage example:

Example 0:

```python
#!/usr/bin/env python

from math import pi

from pita.walker import Pita, walker_log

# Provide stub data
stub = {
    "circles": [
        {"radius": 4},
        {"radius": 6},
    ]
}


# p will be used to store data, register functions and build output
p = Pita(stub, auto_hide=False)


# Add a function computing new data from existing data
@p.add_job("/circles/*", "radius", "area")
def circle_area(radius):
    return pi * radius**2


# process and write result
status = p.run()

with open("example_0.json", "w+") as f:
    p.json_dump_root(walker_log.dot, file=f)

```

This example computes the area of each provided circle, given their radius.
The `@p.add_job(basepath, input, output)` decorator says the following things:

- "/circles/*" is the basepath. the function will apply on each matching element
- take the *input* existing value "radius"
- store the function *output* "area" in the current circle element, alongside the radius

> **Note**
> This decorator required parameters are basepath, input(s), output(s).
> If input or/and output contain multiple values, they are passed as a list.

The output of this program is a json object, written to example_0.json:

```json
{
  "circles": [
    {
      "radius": 4,
      "area": 50.26548245743669
    },
    {
      "radius": 6,
      "area": 113.09733552923255
    }
  ]
}


```

The basepath, inputs and outputs use a notation from the UNIX world. Here is a decomposition of the basepath "/circles/*":

- The first "/" means "root", it is the base element given to the Pita object.
- "circles" names the contained object as if it was a folder.
- The following slash means to go down in the data hierarchy
- "*" (wildcard) means to process all children, or sub-folders, of circles

inputs and outputs are relative to the basepath. Here, "radius" and "area" are understood for each circle.
The 2nd argument is the function input. If several inputs are required, this 2nd argument is a list.
The 3rd argument is the function output. The returned value will be added to each circle once the circle_area() function is computed. As the input, it may be a list.

The resulting data is computed during `p.run()` and is stored in a file in the `with open("example_0.json", "w+") as f:` block.

### Chaining functions

For now, Pita does not provide anything useful. The advantages come when functions are *implicitly* chained, depending on their inputs and their outputs.

Example 1:

```python
#!/usr/bin/env python

from math import pi

from pita.walker import Pita, walker_log

# Compared to the previous example, Add the height
stub = {
    "circles": [
        {"radius": 4, "height": 4},
        {"radius": 6, "height": 5},
    ]
}


p = Pita(stub, auto_hide=False)


@p.add_job("/circles/*", "radius", "area")
def circle_area(radius):
    return pi * radius**2


# area is computed by the previous function, height is given in the stub
@p.add_job("/circles/*", ["area", "height"], "volume")
def cylinder_volume(area, height):
    return area * height


status = p.run()

with open("example_1.json", "w+") as f:
    p.json_dump_root(walker_log.dot, file=f)


```

A 2nd function uses the 1st function output. The order these functions are declared is not important as ```p.run()``` will try to solve all the functions: If a function may not be computed yet because one of its input does not exist yet, it waits for this value to be ready and computes it later.

Output:

```json
{
  "circles": [
    {
      "radius": 4,
      "height": 4,
      "area": 50.26548245743669,
      "volume": 201.06192982974676
    },
    {
      "radius": 6,
      "height": 5,
      "area": 113.09733552923255,
      "volume": 565.4866776461628
    }
  ]
}
```

## More complex examples

From now on, the examples will compute data about a *pièce montée* cake consisting of several cylindrical cakes, each one having a smaller radius than the one below.

Example 2:

```python
#!/usr/bin/env python

from math import pi

from pita.walker import Pita, walker_log

# Do not specify layers dimensions but give stub values to compute them
stub = {
    # the first layer radius is the plate radius
    "plate-radius": 10,
    # the first layer height
    "first-height": 5,
    # each layer radius and height is smaller than the previous one by this
    # factor
    "decrease-factor": 0.8,
    # iteratively decrease layers dimensions, until this limit is reached
    "min-radius": 4,
}

# put the stub in the "stub" child node to separate it from the result
p = Pita({"stub": stub})


# keep the old functions, working on the computed radii and heights
@p.add_job("/layers/*", ["area", "height"], "volume")
def cylinder_volume(area, height):
    return area * height


@p.add_job("/layers/*", "radius", "area")
def circle_area(radius):
    return pi * radius**2


# iteratively compute radii and heights instead of specifiying it in the stub
@p.add_job(
    "/stub",
    ["plate-radius", "first-height", "decrease-factor", "min-radius"],
    "/layers",
)
def circles(radius, height, decrease_factor, min_radius):
    layers = []
    while radius >= min_radius:
        layers.append({"radius": round(radius, 1), "height": round(height, 1)})
        radius *= decrease_factor
        height *= decrease_factor
    return layers


###############################################################################
# The following examples come here
###############################################################################


status = p.run()

with open("example_2.json", "w+") as f:
    p.json_dump_root(walker_log.dot, file=f)

```

There are few changes but no new keyword for now. Instead of giving directly radii and heights, these are computed from the "stub" values and put in the "layers" node.

The functions are declared in the reverse order they are used to show that, as long as ```p.run()``` is called after all function declarations, Pita manage the call tree.

Output:

```json
{
  "layers": [
    {
      "radius": 10,
      "height": 5,
      "area": 314.1592653589793,
      "volume": 1570.7963267948967
    },
    {
      "radius": 8.0,
      "height": 4.0,
      "area": 201.06192982974676,
      "volume": 804.247719318987
    },
    {
      "radius": 6.4,
      "height": 3.2,
      "area": 128.67963509103794,
      "volume": 411.77483229132145
    },
    {
      "radius": 5.1,
      "height": 2.6,
      "area": 81.71282491987051,
      "volume": 212.45334479166334
    },
    {
      "radius": 4.1,
      "height": 2.0,
      "area": 52.81017250684442,
      "volume": 105.62034501368883
    }
  ]
}
```

Note that the stub values are not in the output json because `auto_hide=False` is not provided to the Pita init. The default behaviour is to hide the user provided data (the stub) and only show the computed values.

### List operations

When using lists, from within an elemnent, it is possible to:

- retrieve the length of the list with the "#" variable
- retrieve the index of the current element with the "!" variable
- reference the previous and the next elements, respectively with the "-" and "+" variables.

#### Count and indices
Using the element index and the list size may be useful to add a specific treatment to an element. For example to add a candle only the top layer:

Example 3:

```python
@p.add_job("/layers/*", ["#", "!"], "has-candle")
def candle(count, index):
    return index == count - 1

```

All layers now have a "has-candle" attribute, set to false to all layers except for the last one.

#### previous and next

This is a bit more complex because referencing the next layer has no sense from the last one (and referencing the previous layer from the first layer neither).

To handle this case, the basepath ("/layers/*" in this case) may have a fallback value.

The following example computes the accumulated weight of everything above it.
The last layer supports the weight of the candle, the previous one supports the last layer and the candle, etc.

So this example adds the candle weight in the stub and computes the accumulated weight of each:

Example 4:

```python
#!/usr/bin/env python

from math import pi

from pita.walker import Pita, walker_log


stub = {
    "plate-radius": 10,
    "first-height": 5,
    "decrease-factor": 0.8,
    "min-radius": 4,
    # add the candle weight with the same name as the layers accumulated weight
    "accumulated-weight": 4,
    # and the cake density to compute the weight from the volume
    "cake-density": 1,
}

p = Pita({"stub": stub})


@p.add_job("/layers/*", ["area", "height"], "volume")
def cylinder_volume(area, height):
    return area * height


@p.add_job("/layers/*", "radius", "area")
def circle_area(radius):
    return pi * radius**2


@p.add_job(
    "/stub",
    ["plate-radius", "first-height", "decrease-factor", "min-radius"],
    "/layers",
)
def circles(radius, height, decrease_factor, min_radius):
    layers = []
    while radius >= min_radius:
        layers.append({"radius": round(radius, 1), "height": round(height, 1)})
        radius *= decrease_factor
        height *= decrease_factor
    return layers


@p.add_job("/layers/*", ["#", "!"], "has-candle")
def candle(count, index):
    return index == count - 1


# compute each layer own weight
@p.add_job("/layers/*", ["volume", "/stub/cake-density"], "weight")
def compute_own_weight(volume, density):
    return volume * density


# /stub is a fallback when /layers/N+1 does not exist
# in this case /layers/+/accumulated-weight is replaced with
# /stub/accumulated-weight
@p.add_job(
    ["/layers/*", "/stub"],
    ["weight", "+/accumulated-weight"],
    "accumulated-weight",
)
def compute_accumulated_weight(own_weight, next_accumulated_weight):
    return own_weight + next_accumulated_weight


status = p.run()

with open("example_4.json", "w+") as f:
    p.json_dump_root(walker_log.dot, file=f)

```

The compute_own_weight() function simply computes each layer own weight taking each layer "volume" value and the common cake density from the stub (the two inputs are grouped in a list).

The compute_accumulated_weight() take the accumulated_weight of the next layer "+/accumulated_weight" or if it is impossible to get it, falls back to /stub/accumulated_weight which has been set to the candle weight.

#### Wildcard in a function input

In the previous examples, the wildcard "*" was only used in the basepath and means "apply this function to all matching nodes".
When used in an input, the wildcard means "gather all matching values and give them as a list to the function".

The following example uses this to compute the total *glaçage* surface required to cover all layers borders:

Example 5:

```python
#!/usr/bin/env python

from math import pi

from pita.walker import Pita, walker_log


stub = {
    "plate-radius": 10,
    "first-height": 5,
    "decrease-factor": 0.8,
    "min-radius": 4,
    "accumulated-weight": 4,
    "cake-density": 1,
}

p = Pita({"stub": stub})


@p.add_job("/layers/*", ["area", "height"], "volume")
def cylinder_volume(area, height):
    return area * height


@p.add_job("/layers/*", "radius", "area")
def circle_area(radius):
    return pi * radius**2


@p.add_job(
    "/stub",
    ["plate-radius", "first-height", "decrease-factor", "min-radius"],
    "/layers",
)
def circles(radius, height, decrease_factor, min_radius):
    layers = []
    while radius >= min_radius:
        layers.append({"radius": round(radius, 1), "height": round(height, 1)})
        radius *= decrease_factor
        height *= decrease_factor
    return layers


# compute the perimeter of each layer to feed the glacage() function
@p.add_job("/layers/*", "radius", "perimeter")
def perimeter(radius):
    return radius * pi


# take a list of all perimeters and a list of all heights
@p.add_job("/layers", ["*/perimeter", "*/height"], "/glacage")
def glacage(perimeter, height):
    return sum([p*h for p, h in zip(perimeter, height)])


status = p.run()

with open("example_5.json", "w+") as f:
    p.json_dump_root(walker_log.dot, file=f)

```


#### Parent references

As in the Unix path, It is possible to reference a parent using "..".
Note that from a list element, it is mandatory to use "../.." to reference a sibling of the list as it is "common-base/list/Nth-element": common-base is not the direct parent but the grand parent of Nth-element.

Self referencing with "." is not supported as no use case has been found for now.

Referencing the parent may be useful in some cases beyond the current trivial examples.
Here is an example that should use the root "/" keyword instead :

Example 6:

```python
#!/usr/bin/env python


from pita.walker import Pita, walker_log


stub = {
    "plate-radius": 10,
    "first-height": 5,
    "decrease-factor": 0.8,
    "min-radius": 4,
}

p = Pita({"stub": stub})


@p.add_job(
    "/stub",
    ["plate-radius", "first-height", "decrease-factor", "min-radius"],
    "/layers",
)
def circles(radius, height, decrease_factor, min_radius):
    layers = []
    while radius >= min_radius:
        layers.append({"radius": round(radius, 1), "height": round(height, 1)})
        radius *= decrease_factor
        height *= decrease_factor
    return layers


# Use "../../stub/min-radius" instead of "/stub/min-radius" for the example
@p.add_job("/layers/*", ["radius", "../../stub/min-radius"], "coherent")
def previous_limit_test_worked(radius, stub_radius):
    return radius >= stub_radius


status = p.run()

with open("example_6.json", "w+") as f:
    p.json_dump_root(walker_log.dot, file=f)


```

## Log

An application using the Pita way of chaining functions may not work on the first try, for example:

- If there is a typo in a variable name when it is produced or used (eg a fonction produces "/foo/bar" and another function waits for "/foo/baz"), Pita won't recognize this and run() will report an error as it was impossible to solve the function requiring "/foo/baz" because "/foo/baz" was never produced.
- A complete workflow may become quiet complex and be incoherent during the development.

In this cases, one may want to:

- print the run() status as a string (1)
- print a list of stalled functions (2)
- write the current data and functions tree to a Graphviz dot diagram (3)

```python
status = p.run()

if status != Pita.OK:
    walker_log.warn("status:", p.status_str(status))  # 1
    p.print_stalled_jobs(walker_log.info)  # 2
    p.write_dot("final")  # 3
```

Printing the stalled functions uses an heuristic on the number of missing inputs and outputs parent directories to print the functions requiring the least data to work at the bottom of the list.
Each input is prefixed by "<". Each output is prefixed by ">", the input group is prefixed by "<<<" and the output gfroup is prefixed by ">>>". The "." represents where Pita is when it gives up.

In the following example, are_you_sure() depends on "coherent __!__" but only "coherent" is produced, this is a typo leading to an unsolved case :

Example 7

```python
#!/usr/bin/env python

from pita.walker import Pita, walker_log
from pita.utils import Log

walker_log.verbose_level = Log.DBG

stub = {
    "plate-radius": 10,
    "first-height": 5,
    "decrease-factor": 0.8,
    "min-radius": 4,
}

p = Pita({"stub": stub})


@p.add_job(
    "/stub",
    ["plate-radius", "first-height", "decrease-factor", "min-radius"],
    "/layers",
)
def circles(radius, height, decrease_factor, min_radius):
    layers = []
    while radius >= min_radius:
        layers.append({"radius": round(radius, 1), "height": round(height, 1)})
        radius *= decrease_factor
        height *= decrease_factor
    return layers


# Use "../../stub/min-radius" instead of "/stub/min-radius" for the example
@p.add_job("/layers/*", ["radius", "../../stub/min-radius"], "coherent")
def previous_limit_test_worked(radius, stub_radius):
    return radius >= stub_radius


@p.add_job("/layers/*", "coherent!", "really-coherent")
def are_you_sure(coherent):
    return coherent


status = p.run()

if status != Pita.OK:
    walker_log.warn("status:", p.status_str(status))
    p.print_stalled_jobs(walker_log.info)
    p.write_dot("final")
else:
    with open("output_6.json", "w+") as f:
        p.json_dump_root(walker_log.dot, file=f)

```

the output contains the verbose output and the stalled functions at the end :

```
§ Job 9 (are_you_sure) [1]: /layers/* .  <<<  . coherent! >>>  . /really-coherent
§ Job 10 (are_you_sure) [1]: /layers/* .  <<<  . coherent! >>>  . /really-coherent
§ Job 11 (are_you_sure) [1]: /layers/* .  <<<  . coherent! >>>  . /really-coherent
§ Job 12 (are_you_sure) [1]: /layers/* .  <<<  . coherent! >>>  . /really-coherent
§ Job 13 (are_you_sure) [1]: /layers/* .  <<<  . coherent! >>>  . /really-coherent

```
"/layers/* .", with the dot at the end, means that Pita successfully reached each layer but ". coherent!" means that Pita was not able to gather the "coherent!" value and stalled.

### Verbose output, under the hood

The verbose output printed before the list of stalled jobs is caused by

```python
walker_log.verbose_level = Log.DBG
```

setting the Pita verbose level to DBG. All Pita messages having a level equal or greater than DBG is printed. The default level is ERROR.

Basepath, function In and function Out have already been discussed here.
Treewalker has not. It is an object parsing a path (from a basepath, or a function in/out).

A basepath has a chain of TreeWalkers, when a treewalker returns that it can't be computed, the basepath tries the next one if any, or fails. This is the case in the example 4, the treewalker walking over "/layers/+/accumulated-weight" failed for the last layer, the basepath switched to the next treewalker walking over "/stub".

A function In/Out may also contain several treewalkers but they are not chained, if a single input or output is not reachable, it fails.

When a wildcard is encountered in the basepath, the basepath splits itself and the calling job to a new basepath and a new job for each matching child.

When a wildcard is encountered in a function input, the function job forks this input and put all new inputs in a list, to be provided as a single argument to the function.

### Log levels

A log object is instanciated for the pita walker and is used as in the example above. Several Log objects may be created and have different levels. Levels are

```python
NONE = -1  # no output at all
DOT = 0  # write dot output
TEST = 1  # show currently debugging traces (only for temporary debug)
ERROR = 2  # fatal error, it should stop
WARN = 3  # non fatal, it may continue but there might be a catch
INFO = 4  # non error but important message
DBG = 5  # only for debug, coarse trace
VDBG = 6  # only for debug, fine trace
VVDBG = 7  # only for debug, complete trace
ACTION = 8  # action prefix marker style
```

The (Graphviz) dot output disables any CLI coloring as it is intended to be written to a file, and color characters are not to be saved to a file.

### Dot

The [Graphviz](https://graphviz.org/) output is a tree of all values as tree nodes.

The stalled jobs are also printed. For now, their pending inputs are linked to the node they are currently watching. One of this project TODOs is to add expected nodes (which do not exist for now) and link jobs to them, it will be easier to see the first faulty job.

Pita does not produce images from the dot textual outputs, you can do it with eg:

```sh
dot graph.gv -Tpng > image.png
```

As the graph is rapidly large, some dot options may give a better result but none of the tested options gives a perfect output for all workflows so none is provided here.

## Internal work

```plantuml
title simplified run loop

participant Pita
participant Job
participant BasePath
participant FunctionIo
participant TreeWalker
participant Node

alt if todo list empty
    Pita -> Pita: exit success
end
alt if pending list empty
    Pita -> Pita: exit failure
end
loop on each pending job
    Pita -> Job: travel()

    alt parse basepath
        Job -> BasePath: travel()
        BasePath -> TreeWalker: travel()
        TreeWalker --> BasePath: status, new tree walkers
        alt split
            BasePath -> Job: fork()
        end
        alt deadend
            BasePath -> Job: next_basepath()
        end
        alt goal
            BasePath -> Job: parse_function_io()
        end
    else
        Job -> FunctionIo: travel()
        alt split
            FunctionIo -> FunctionIo: fork()
        end
        alt deadend
            FunctionIo -> Pita: exit_failure()
        end
        alt goal
            Job -> Node: compute()
            Node -> TreeWalker: update()
            TreeWalker -> FunctionIo: update()
            FunctionIo -> Job: update()
            Job -> Pita: move stalled jobs to pending list
            Job -> Pita: remove current job from todo list
        end
    end
    Pita -> Pita: remove current job from pending list
end
```

## Chooser

A small module allowing to find the best combination of several parameters, giving their range and the weight of each parameter.
Chooser exposes `chooser_log` which provides debugging information.

See `test/test_chooser.py` for a usage example.

## TODO

- dot output: add nodes that are not created yet to link the stalled functions
- check for cycles
- allow to multithread jobs (using a pool of processes?)
- add mypy and pylint to CI process

