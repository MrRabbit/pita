#!/usr/bin/env python3

from pita.utils import print_start, indent_log, log_action
from .walkerlog import log
from .treewalker import TravelStatus, TreeWalker
from .memory import fork
from .node import Node
from .debugassert import dbg_assert


class FunctionIO:
    """
    a FunctionIo references a single user defined input or output.

    If a function input  is split, the target is a list of the values of the
    gathered nodes instead of the value of a single node.
    """

    def __init__(
        self,
        watcher,
        walkers,
        *,
        allow_split,
        has_split=False,
        first=True,
        follow=False,
    ):
        self.uniq = FunctionIO.get_id()
        self.watcher = watcher
        self.walkers = walkers
        self.pending = list(walkers)
        self.allow_split = allow_split
        self.has_split = has_split
        self.first = first
        self.follow = follow
        for i in walkers:
            i.watcher = self

    def unlink(self):
        for i in self.walkers:
            i.unlink()

    @indent_log
    def travel(self):
        ret = TravelStatus(TravelStatus.STALL)
        for i in list(self.pending):  # iterate on a copy to allow erase
            s, new_walkers = i.travel()
            i.print_indented(
                log.vvdbg,
                pre=f"function arg travel {TravelStatus.to_str(s)}",
                indent=2,
            )
            if s == TravelStatus.DEADEND:
                return TravelStatus.DEADEND
            elif s == TravelStatus.DEADEND_RETRY:
                if self.first:
                    return TravelStatus.DEADEND_RETRY
                return TravelStatus.DEADEND
            elif s == TravelStatus.SPLIT:
                dbg_assert.check(
                    self.allow_split, "may not split a function output"
                )
                self.has_split = True
                self.walkers.remove(i)
                self.pending.remove(i)
                if not new_walkers:  # enpty split will prevent further work
                    self.watcher.error(f"split to nothing {self}")
                self.walkers += new_walkers
                self.pending += new_walkers

                self.watcher.update(self)
            elif s == TravelStatus.GOAL:
                self.pending.remove(i)
            if s is not TravelStatus.STALL:
                ret.set_max(TravelStatus.MOVE)
            if ret.value is not TravelStatus.STALL:
                self.first = False
        if not self.pending:
            return TravelStatus.GOAL
        return ret.value

    def update(self, walker):
        log_action(log.vvdbg, "update", self, walker in self.pending)
        if walker not in self.pending:
            self.pending.append(walker)
        self.watcher.update(self)

    def set_rel_root(self, rel_root):
        for i in self.pending:
            i.set_rel_root(rel_root)

    def heuristic(self):
        return sum([i.heuristic() for i in self.walkers])

    # print functions

    uniq_id = 0

    @classmethod
    def get_id(cls):
        cls.uniq_id += 1
        return cls.uniq_id

    def signature(self):
        return f"FunctionIO {self.uniq}"

    def print_indented(self, log_level, indent=0, *, pre=""):
        print_start(log_level, self, indent, pre)
        for i in self.walkers:
            i.print_indented(log_level, indent + 1)

    def one_liner(self):
        return ", ". join([i.one_liner() for i in self.walkers])


class FunctionIn(FunctionIO):
    """
    A FunctionIO dedicated to function input
    """

    def __init__(self, *args, has_split=False, **kwargs):
        super().__init__(
            *args,
            **kwargs,
            allow_split=True,
            has_split=has_split,
        )

    @classmethod
    def from_init(cls, watcher, root, paths, follow):
        if isinstance(paths, str):
            paths = [paths]
        return [
            FunctionIn(
                watcher, [TreeWalker.from_init(None, root, root, i, follow)]
            )
            for i in paths
        ]

    def fork(self, rel_root):
        dbg_assert.check(
            not self.has_split,
            "may not fork a basepath if the function has already split",
        )
        return FunctionIn(None, fork(self.walkers, rel_root), has_split=False)

    def value(self):
        if self.has_split:
            return [i.value() for i in self.walkers]
        dbg_assert.check(
            len(self.walkers) == 1,
            "expected a single walker if the walker has not split",
        )
        return self.walkers[0].value()

    # print functions

    def signature(self):
        return f"FunctionIn {self.uniq}"

    def __str__(self):
        return self.signature()

    def __repr__(self):
        return self.__str__()


class FunctionOut(FunctionIO):
    """
    A FunctionIO dedicated to function output

    A function output may not split. Its target node is the parent of the user
    defined node. When the function is computed, a new child is insterted in
    the target children list.
    """

    def __init__(self, watcher, walkers, target_name, first=True):
        self.target_name = target_name
        super().__init__(watcher, walkers, allow_split=False, first=first)

    @classmethod
    def from_init(cls, watcher, root, paths, follow):
        ret = []
        if isinstance(paths, str):
            paths = [paths]
        walkers = [
            TreeWalker.from_init(None, root, root, i, follow) for i in paths
        ]
        for i in walkers:
            last = i.pop_back()
            ret.append(FunctionOut(watcher, [i], last.name, follow))
        return ret

    def fork(self, rel_root):
        dbg_assert.check(not self.has_split, "a function output may not split")
        return FunctionOut(
            None, fork(self.walkers, rel_root), self.target_name
        )

    def set_value(self, value, **kwargs):
        dbg_assert.check(
            len(self.walkers) == 1,
            "a function output may only fill a single node",
        )
        n = Node(self.walkers[0].node, self.target_name, self.follow)
        n.set_value(value, **kwargs)

    # print functions

    def signature(self):
        return f"FunctionOut {self.uniq} -> {self.target_name}"

    def one_liner(self):
        return super(FunctionOut, self).one_liner() + f"/{self.target_name}"

    def __str__(self):
        return self.signature()

    def __repr__(self):
        return self.__str__()
