#!/usr/bin/env python3

from pita.utils import print_start, indent_log, log_action, highlight
from .folder import Folder
from .debugassert import dbg_assert
from .walkerlog import log


class TravelStatus:
    STALL = 0
    MOVE = 1
    SPLIT = 2
    GOAL = 3
    DEADEND = 4
    DEADEND_RETRY = 5  # when a functionio needs to go back to the basepath

    def __init__(self, value):
        self.value = value

    def set_max(self, value):
        if isinstance(value, TravelStatus):
            value = value.value
        if value > self.value:
            self.value = value

    @classmethod
    def to_str(cls, value):
        if isinstance(value, TravelStatus):
            value = value.value
        status_to_str = {
            TravelStatus.STALL: "stall",
            TravelStatus.MOVE: "move",
            TravelStatus.SPLIT: "split",
            TravelStatus.GOAL: "goal",
            TravelStatus.DEADEND: "deadend",
            TravelStatus.DEADEND_RETRY: "deadend-retry",
        }
        return status_to_str[value]

    def __str__(self):
        return self.to_str(self.value)


class TreeWalker:
    """
        A TreeWalker contains a semantic list of folders (the semantic view of
    a node) and tries to reach a target node.

        A TreeWalker may stall on a node because it does not exist yet or is
    flagged as not ready.

        A TreeWalker may also reach a dead end (eg with the path "foo/0/-": -1
    is an invalid index).

        A TreeWalker may also split when encountering a wildcard "*": in this
    case, it builds a list of the new TreeWalkers meant to replace self.

        A treewalker has no set_value() function as the only goal of
    intercepting a Node.get_value() call is to replace the node value by a
    reference but an output may not be a reference
    """

    def __init__(self, watcher, abs_root, rel_root, folders, done, follow):
        self.uniq = TreeWalker.get_id()
        self.watcher = watcher
        self.root = abs_root
        self.node = rel_root
        self.pending = list(folders)
        self.done = list(done)
        self.ref = None
        self.follow = follow
        self.node.watch(self)
        self.travel_by_type = {
            Folder.GENERIC: self.travel_generic,
            Folder.ROOT: self.travel_root,
            Folder.PARENT: self.travel_parent,
            Folder.WILDCARD: self.travel_wildcard,
            Folder.PREVIOUS: self.travel_previous,
            Folder.NEXT: self.travel_next,
            Folder.COUNT: self.travel_reference,
            Folder.INDEX: self.travel_reference,
            Folder.REFERENCE: self.travel_reference,
        }

    def unlink(self):
        self.node.unwatch(self)

    @classmethod
    def from_init(cls, watcher, abs_root, rel_root, path, follow):
        """build a treewalker taking a single path as string"""
        return TreeWalker(
            watcher, abs_root, rel_root, Folder.from_str(path), [], follow
        )

    def clone(self):
        return self.fork(self.node)

    def fork(self, rel_root):
        return TreeWalker(
            self.watcher,
            self.root,
            rel_root,
            self.pending,
            list(self.done),
            self.follow,
        )

    @indent_log
    def travel(self):
        ret = TravelStatus(TravelStatus.STALL)
        while True:
            if not self.pending:
                log_action(log.vvdbg, f"{self.signature()}: goal")
                self.node.unwatch(self)
                return TravelStatus.GOAL, []
            if not self.node.ready:
                log_action(log.vvdbg, f"{self.signature()}: not ready")
                break
            cur = self.pending[0]
            log_action(
                log.vvdbg, f"{self.signature()}: travel {cur.type_str()}"
            )
            child, status, splits = self.travel_by_type[cur.folder_type](cur)
            log_action(
                log.vvdbg,
                f"{self.signature()}: "
                f"from <{cur.type_str()}> {cur.name}: "
                "travel status: "
                + highlight(f"{TravelStatus.to_str(status)} ", bold=True),
            )
            if status == TravelStatus.DEADEND or status == TravelStatus.GOAL:
                self.pending.pop(0)
                self.node.unwatch(self)
                # return DEADEND_RETRY if the first move is a dead end
                if (
                    ret.value == TravelStatus.STALL
                    and status == TravelStatus.DEADEND
                ):
                    status = TravelStatus.DEADEND_RETRY
                return status, []
            elif status == TravelStatus.SPLIT:
                self.node.unwatch(self)
                self.advance()
                return status, [
                    TreeWalker(
                        self.watcher,
                        self.root,
                        i,
                        self.pending,
                        self.done,
                        self.follow,
                    )
                    for i in splits
                ]
            elif status == TravelStatus.MOVE:
                ret.set_max(status)
                self.node.unwatch(self)
                self.node = child
                self.node.watch(self)
                self.advance()
            else:
                return ret.value, []

    def set_rel_root(self, rel_root):
        if self.node:
            self.node.unwatch(self)
        self.node = rel_root
        self.node.watch(self)

    def advance(self):
        self.done.append(self.pending.pop(0))

    def travel_generic(self, folder):
        child = self.node.child(folder.name)
        if child:
            return child, TravelStatus.MOVE, []
        return None, TravelStatus.STALL, []

    def travel_root(self, folder):
        return self.root, TravelStatus.MOVE, []

    def travel_parent(self, folder):
        dbg_assert.check(
            self.node.parent,
            "travelling to parent but this node has no parent",
        )
        return self.node.parent, TravelStatus.MOVE, []

    def travel_wildcard(self, folder):
        dbg_assert.check(
            self.node.is_iterable(),
            "travelling a wildcard but the current node is not iterable",
        )
        return self.node, TravelStatus.SPLIT, self.node.children

    def travel_previous(self, folder):
        return self.travel_neighbour(folder, -1)

    def travel_next(self, folder):
        return self.travel_neighbour(folder, 1)

    def travel_reference(self, folder):
        dbg_assert.check(
            len(self.pending) == 1,
            f"{folder.name} is a reference and should be the last token "
            "of a path",
        )
        if folder.folder_type == Folder.REFERENCE:
            self.ref = self.node
        else:
            dbg_assert.check(
                self.node.parent,
                "requesting a list reference but this node has no parent",
            )
            dbg_assert.check(
                self.node.parent.is_iterable(),
                "requesting a reference but the parent is not iterable",
            )
            if folder.folder_type == Folder.INDEX:
                self.ref = self.node.index()
            else:
                self.ref = self.node.parent.size()
        return None, TravelStatus.GOAL, []

    def travel_neighbour(self, folder, offset):
        dbg_assert.check(
            self.node.parent,
            "requesting a sibling node but this node has no parent",
        )
        dbg_assert.check(
            self.node.parent.is_iterable(),
            "requesting a sibling node but the parent is not iterable",
        )
        idx = self.node.index() + offset
        total = self.node.parent.size()
        if idx < 0 or idx >= total:
            return None, TravelStatus.DEADEND, []
        return self.node.parent.child(idx), TravelStatus.MOVE, []

    def value(self):
        if self.pending:
            log.error(
                "get TreeWalker value but it is still pending:",
                self,
                self.pending,
            )
        if self.ref is None:
            return self.node.value()
        return self.ref

    def pop_back(self):
        dbg_assert.check(
            self.pending,
            "a function out should contain at least the destination node name",
        )
        return self.pending.pop(-1)

    @indent_log
    def update(self, node):
        if self.follow is not None:
            log.set_tmp_level(self.follow)
        log_action(log.vvdbg, "update", self, self.watcher)
        self.watcher.update(self)
        if self.follow is not None:
            log.restore_level()

    def heuristic(self):
        return 0 if self.done else len(self.pending)

    # print functions

    uniq_id = 0

    @classmethod
    def get_id(cls):
        cls.uniq_id += 1
        return cls.uniq_id

    def signature(self):
        return (
            f"TreeWalker {self.uniq} {Folder.readlink(self.done)} . "
            f"{Folder.readlink(self.pending)} ({self.node})"
        )

    def print_indented(self, log_level, indent=0, *, pre=""):
        print_start(log_level, self, indent, pre)

    def one_liner(self):
        return (
            f"{Folder.readlink(self.done)} . {Folder.readlink(self.pending)}"
        )

    def __str__(self):
        return self.signature()

    def __repr__(self):
        return self.__str__()
