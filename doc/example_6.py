#!/usr/bin/env python

from pita.walker import Pita, walker_log
from pita.utils import Log

walker_log.verbose_level = Log.DBG

stub = {
    "plate-radius": 10,
    "first-height": 5,
    "decrease-factor": 0.8,
    "min-radius": 4,
}

p = Pita({"stub": stub})


@p.add_job(
    "/stub",
    ["plate-radius", "first-height", "decrease-factor", "min-radius"],
    "/layers",
)
def circles(radius, height, decrease_factor, min_radius):
    layers = []
    while radius >= min_radius:
        layers.append({"radius": round(radius, 1), "height": round(height, 1)})
        radius *= decrease_factor
        height *= decrease_factor
    return layers


# Use "../../stub/min-radius" instead of "/stub/min-radius" for the example
@p.add_job("/layers/*", ["radius", "../../stub/min-radius"], "coherent")
def previous_limit_test_worked(radius, stub_radius):
    return radius >= stub_radius


status = p.run()

with open("example_6.json", "w+") as f:
    p.json_dump_root(walker_log.dot, file=f)
