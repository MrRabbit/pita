#!/usr/bin/env python3

from pita.utils import print_start, log_action, indent_log

from .debugassert import dbg_assert
from .walkerlog import log


class Node:
    """
    a node contains a single data and 0 or more children.

    When created, it has a name and is not yet ready.
    When it is attributed a value, its type is deduced and it becomes ready.
    When a node becomes ready, it tells all TreeWalkers watching it that they
    may update.
    """

    DICT = 1
    LIST = 2
    LEAF = 3

    PREFIX = 1
    SUFFIX = 2

    def __init__(self, parent, name, follow, hide=False):
        self.uniq = Node.get_id()
        self.watchers = []
        self.ready = False
        self.parent = parent
        self.follow = follow
        self.hide = hide
        if parent:
            self.parent.children.append(self)
        self.children = []
        self.node_type = Node.LEAF
        self.leaf_value = None
        try:
            self.name = int(name)
        except ValueError:
            self.name = name

    def value(self):
        dbg_assert.check(
            self.ready, "taking the value from a node that is not ready"
        )
        if self.node_type == Node.DICT:
            return {n.name: n.value() for n in self.children}
        elif self.node_type == Node.LIST:
            return [n.value() for n in self.children]
        else:
            return self.leaf_value

    def gather(self):
        """same as value but hides hidden children"""
        if self.node_type == Node.DICT:
            return {n.name: n.gather() for n in self.children if not n.hide}
        elif self.node_type == Node.LIST:
            return [n.gather() for n in self.children if not n.hide]
        else:
            return self.leaf_value

    def set_visible_rec(self):
        self.hide = False
        if self.parent and self.parent.hide:
            self.parent.set_visible_rec()

    @indent_log
    def set_value(self, value, *, hide=False, force_visible=False):
        log_action(log.vvdbg, f"create node <{value}> {self.signature()}")
        if force_visible:
            self.set_visible_rec()
        # assert not self.ready TODO put a warning if computation has begun
        if isinstance(value, dict):
            self.node_type = Node.DICT
            for k, v in value.items():
                n = Node(self, k, self.follow, hide=hide)
                n.set_value(v)
        elif isinstance(value, list):
            self.node_type = Node.LIST
            for k, v in enumerate(value):
                n = Node(self, k, self.follow, hide=hide)
                n.set_value(v)
        else:
            self.node_type = Node.LEAF
            self.leaf_value = value
        self.ready = True
        if self.follow is not None:
            log.set_tmp_level(self.follow)
        for i in self.watchers:
            log.vdbg("update node", self)
            i.update(self)
        # When adding a value to an already ready node, update the parent
        if self.parent and self.parent.ready:
            for i in self.parent.watchers:
                log.vdbg("update node parent", self.parent, self)
                i.update(self.parent)
        if self.follow is not None:
            log.restore_level()

    @indent_log
    def watch(self, watcher):
        if watcher not in self.watchers:
            self.watchers.append(watcher)
            log_action(log.vdbg, f"{watcher} watches {self}")

    @indent_log
    def unwatch(self, watcher):
        if watcher in self.watchers:
            self.watchers.remove(watcher)
            log_action(log.vdbg, f"{watcher} unwatches {self}")
        else:
            log_action(log.vdbg, f"unwatch: no {watcher} in {self}")

    def child(self, ref):
        """return child given its index or name, whether self is an index"""
        for i in self.children:
            if not isinstance(ref, type(i.name)):  # TODO remove
                log.error("not the same type", ref, i.name)  # TODO remove
            dbg_assert.check(
                isinstance(ref, type(i.name)),
                f"{ref} and {i.name} may not be compared",
            )
            if i.name == ref:
                return i
        return None

    def clear(self):
        self.children = []

    def is_iterable(self):
        return self.node_type == Node.LIST or self.node_type == Node.DICT

    def index(self):
        """return index of self in the parent list of children"""
        dbg_assert.check(
            self.parent, "taking the index of a node that do not have a parent"
        )
        dbg_assert.check(
            self.parent.is_iterable(),
            "taking the index of a node but its parent is not iterable",
        )
        for idx, i in enumerate(self.parent.children):
            if i is self:
                return idx
        return None

    def size(self):
        dbg_assert.check(
            self.is_iterable(),
            "getting the number of children of a node but is not iterable",
        )
        return len(self.children)

    def travel(self, mode, fct):
        if self.hide:
            return
        if mode == Node.PREFIX:
            fct(self)
        for i in self.children:
            i.travel(mode, fct)
        if mode == Node.SUFFIX:
            fct(self)

    # print functions
    uniq_id = 0

    @classmethod
    def get_id(cls):
        cls.uniq_id += 1
        return cls.uniq_id

    def signature(self):
        type_to_str = {
            Node.DICT: "dict",
            Node.LIST: "list",
            Node.LEAF: "leaf",
        }
        val = (
            f"({self.leaf_value})"
            if (self.ready and self.leaf_value is not None)
            else ""
        )
        return (
            f"Node {self.uniq} <{type_to_str[self.node_type]}> "
            + f"{self.name} {val}"
        )

    def print_indented(self, log_level, indent=0, *, pre=""):
        print_start(log_level, self, indent, pre)
        for i in self.children:
            i.print_indented(log_level, indent + 1)

    def __str__(self):
        return self.signature()

    def __repr__(self):
        return self.__str__()
