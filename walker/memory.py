#!/usr/bin/env python3


def fork(objs, changed):
    """ for a list of objects by forking all objects """
    return [i.fork(changed) for i in objs]
