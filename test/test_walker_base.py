#!/usr/bin/env python3

import unittest

from pita.utils import Log
from pita.walker import Pita
from pita.walker import walker_log


class Base(unittest.TestCase):

    ingredients = {
        "ingredients": [
            {
                "weight": 0.1,  # kg
                "price_per_kg": 10,  # $/kg
                "name": "chocolate",
            },
            {
                "weight": 0.5,  # kg
                "price_per_kg": 1,  # $/kg
                "name": "floor",
            },
        ],
    }

    stub_value = {
        "rank": 0,
        "stub": {
            "size": 10,
            "weight": 0,
            "toppings": ["marmelade", "caramel", "coconut"],
            "accumulated-weight": 10,
        },
        "layers": [
            {},
            {},
        ],
    }

    def setUp(self):
        walker_log.verbose_level = Log.DOT
        self.p = Pita(Base.ingredients)
        self.p.root.set_value(Base.stub_value, hide=True)

    def tearDown(self):
        pass

    def test_simple(self):
        @self.p.add_job("/ingredients/*", ["weight", "price_per_kg"], "price")
        def compute_price(weight, price_per_kg):
            return weight * price_per_kg

        s = self.p.run()
        self.assertEqual(s, Pita.OK)

        data = self.p.root.value()
        self.assertTrue("ingredients" in data)
        self.assertTrue(len(data["ingredients"]) == 2)
        self.assertTrue("price" in data["ingredients"][0])
        self.assertEqual(data["ingredients"][0]["price"], 1)
        self.assertEqual(data["ingredients"][1]["price"], 0.5)

    @unittest.skip("not an automated test")
    def test_output(self):
        self.p.root.print_indented(walker_log.info, pre="result")

    def test_dot_output(self):
        self.p.write_dot("out")

    def test_ref(self):
        @self.p.add_job("/ingredients/*", ["!", "#"], ["idx", "count"])
        def get_ref(idx, cnt):
            return (idx, cnt)

        s = self.p.run()
        self.assertEqual(s, Pita.OK)

        data = self.p.root.value()
        self.assertTrue("ingredients" in data)
        self.assertTrue(len(data["ingredients"]) == 2)
        self.assertEqual(data["ingredients"][0]["idx"], 0)
        self.assertEqual(data["ingredients"][1]["idx"], 1)
        self.assertEqual(data["ingredients"][0]["count"], 2)
        self.assertEqual(data["ingredients"][1]["count"], 2)

    def test_parent(self):
        @self.p.add_job("/stub/weight", ["../toppings"], ["../nb_toppings"])
        def use_parent(toppings):
            return len(toppings)

        s = self.p.run()
        self.assertEqual(s, Pita.OK)

        data = self.p.root.value()
        self.assertTrue("nb_toppings" in data["stub"])
        self.assertEqual(data["stub"]["nb_toppings"], 3)
        self.p.root.print_indented(walker_log.info, pre="result parent")

    def test_index(self):
        @self.p.add_job("/ingredients/0", "weight", "double-weight")
        def double_weight(weight):
            return 2 * weight

        s = self.p.run()
        self.assertEqual(s, Pita.OK)

        data = self.p.root.value()
        self.assertTrue("ingredients" in data)
        self.assertTrue(len(data["ingredients"]) == 2)
        self.assertTrue("double-weight" in data["ingredients"][0])
        self.assertEqual(data["ingredients"][0]["double-weight"], 0.2)

    def test_neighbour(self):
        @self.p.add_job(
            ["/ingredients/0", "/stub"],
            ["-/accumulated-weight"],
            ["accumulated-weight"],
        )
        def double_prev(prev):
            return 2 * prev

        s = self.p.run()
        self.assertEqual(s, Pita.OK)

        data = self.p.root.value()
        self.assertTrue("ingredients" in data)
        self.assertTrue(len(data["ingredients"]) == 2)
        self.assertTrue("accumulated-weight" in data["ingredients"][0])
        self.assertEqual(data["ingredients"][0]["accumulated-weight"], 20)

    def test_neighbour_split(self):
        @self.p.add_job(
            ["/ingredients/*", "/stub"],
            ["-/accumulated-weight", "weight"],
            ["accumulated-weight"],
        )
        def acc_weight(prev, cur):
            return prev + cur

        s = self.p.run()
        self.assertEqual(s, Pita.OK)

        data = self.p.root.value()
        self.assertTrue("ingredients" in data)
        self.assertTrue(len(data["ingredients"]) == 2)
        self.assertTrue("accumulated-weight" in data["ingredients"][1])
        self.assertEqual(data["ingredients"][1]["accumulated-weight"], 10.6)

    def test_arg_split(self):
        @self.p.add_job("/", "ingredients/*/weight", "accumulated-weight")
        def acc_weight_root(weights):
            return sum(weights)

        s = self.p.run()
        self.assertEqual(s, Pita.OK)

        data = self.p.root.value()
        self.assertTrue("accumulated-weight" in data)
        self.assertEqual(data["accumulated-weight"], 0.6)


# FFS
# https://medium.com/@vladbezden/using-python-unittest-in-ipython-or-jupyter-732448724e31
unittest.main(argv=["first-arg-is-ignored"], exit=False)
