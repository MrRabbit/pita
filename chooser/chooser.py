#!/usr/bin/env python3

import pprint

from pita.utils import Log


log = Log(Log.ERROR)


class NoCombinationtFound(Exception):
    """Raised when all combinations have -inf score"""

    pass


class Arg:
    """simple wrapper to tell it is a reference, not a direct value"""

    def __init__(self, name):
        self.name = name


class Param:
    """
    Describe a parameter to be adjusted.

    Name: user defined name which may be referenced using Arg().

    a parameter may either:

    - give an iterable as accepted_values
    - give a fct to produce a value. In this case, accepted_values is a
        function that invalidates this candidates if it returns false

    Priority function: the higher, the bigger the priority. Each candidate
    priority is the sum of each parameter.priority(parameter.value).

    args: list of arguments to pass to fct. An argument ma be a user provided
    value or a reference to the current value of another parameter for one
    candidate.
    """

    def __init__(self, name, accepted_values, priority, fct=None, *args):
        self.name = name
        self.accepted_values = accepted_values
        self.priority = priority
        self.fct = fct
        self.args = args

    def compute(self, other_args):
        x = self.fct(
            *[
                other_args[i.name] if isinstance(i, Arg) else i
                for i in self.args
            ]
        )
        return x if self.accepted_values(x) else None


class Chooser:
    """
    take a list of parameters, build a list of candidates and choose the best
    combination
    """

    def __init__(self, params):
        candidates = []
        for self_idx, param in enumerate(params):
            if param.fct:
                candidates = [
                    c
                    + [
                        param.compute(
                            {
                                other_param.name: c[other_idx]
                                for other_idx, other_param in enumerate(
                                    params[:self_idx]
                                )
                            }
                        )
                    ]
                    for c in candidates
                ]
            else:
                if candidates:
                    candidates = [
                        j + [i]
                        for i in param.accepted_values
                        for j in candidates
                    ]
                else:
                    candidates = [[i] for i in param.accepted_values]
        scores = [
            sum(
                [
                    p.priority(v) if v is not None else float("-inf")
                    for p, v in zip(params, c)
                ]
            )
            for c in candidates
        ]
        best = max(enumerate(scores), key=lambda x: x[1])[0]
        self.values = candidates[best]
        pp = pprint.PrettyPrinter(indent=4)
        log.dbg(pp.pformat(candidates))
        log.dbg(pp.pformat(scores))
        if max(scores) == float("-inf"):
            raise NoCombinationtFound
