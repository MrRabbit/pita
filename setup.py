import setuptools

with open("README.md", "r") as fh:
    description = fh.read()

setuptools.setup(
    name="pita",
    version="0.0.1",
    author="MrRabbit",
    author_email="mickael@mrrabb.it",
    packages=["walker", "chooser", "utils"],
    description="build data from data and functions",
    long_description=description,
    long_description_content_type="text/markdown",
    url="https://TODO",
    license="GPLv3",
    python_requires=">=3.9",
    install_requires=["highlightcli"],
)
