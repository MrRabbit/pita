#!/usr/bin/env python3

import unittest

from pita.utils import Log
from pita.walker import Pita
from pita.walker import walker_log


class Chained(unittest.TestCase):

    stub = {
        "stub": {
            "half-layers-nb": 2,
            "accumulated-weight-g": 200,
            "z-top-mm": 0,
        }
    }

    def setUp(self):
        walker_log.verbose_level = Log.DOT
        self.p = Pita()
        self.p.root.set_value(Chained.stub, hide=True)

    def tearDown(self):
        pass

    def test_create_lists(self):

        # declare functions in reverse order of execution to test

        # test arg split
        @self.p.add_job(
            "/layers/*",
            "pilars/*/diam-mm",
            "sum-pillars-surface-mm2",
        )
        def compute_pilars_total_surface(diams):
            # use a wrong formula to avoid floating point assert
            return sum(diams)

        # test +
        @self.p.add_job(
            ["/layers/*", "/stub"],
            ["+/z-top-mm", "height-mm"],
            "z-top-mm",
        )
        def compute_z_top(prev_z, height):
            return prev_z + height

        # test ! and #
        @self.p.add_job(
            "/layers/*",
            ["accumulated-weight-g", "!", "#", "/stub/layers-nb"],
            "pilars",
        )
        def compute_pilars(acc_weight, idx, nb, nb_layers):
            return [{"diam-mm": acc_weight / 100}] * nb

        # test -
        @self.p.add_job(
            ["/layers/*", "/stub"],
            ["weight-g", "-/accumulated-weight-g"],
            "accumulated-weight-g",
        )
        def compute_acc_w(own, prev):
            return own + prev

        # test function in as list
        @self.p.add_job("/layers/*", ["diameter-mm", "height-mm"], "weight-g")
        def compute_weight(diam, height):
            # assuming a density of pi/4 g/mm^3
            return diam * diam * height

        # test list split
        @self.p.add_job("/layers/*", "diameter-mm", "height-mm")
        def compute_height(diam):
            return diam / 10

        # test node creation with list value
        @self.p.add_job("/", "stub/layers-nb", "layers")
        def create_layers(layers_nb):
            return [{"diameter-mm": (i + 1) * 10} for i in range(layers_nb)]

        # compute other functions from this one
        @self.p.add_job("/stub", "half-layers-nb", "layers-nb")
        def compute_layers(half):
            return half * 2

        s = self.p.run()

        self.p.write_dot("chained")
        self.p.root.print_indented(walker_log.info, pre="result")

        self.assertEqual(s, Pita.OK)

        data = self.p.root.value()
        self.assertTrue("layers" in data)
        self.assertEqual(len(data["layers"]), 4)
        acc_w = 200
        for i in range(4):
            dl = data["layers"][i]
            self.assertTrue("height-mm" in dl)
            self.assertTrue("diameter-mm" in dl)
            self.assertTrue("weight-g" in dl)
            self.assertTrue("accumulated-weight-g" in dl)
            self.assertTrue("sum-pillars-surface-mm2" in dl)
            d = dl["diameter-mm"]
            self.assertEqual(d, (i + 1) * 10)
            self.assertEqual(dl["height-mm"], i + 1)
            # TODO test total piar surface value
            w = d * d * d / 10
            acc_w += w
            self.assertEqual(dl["weight-g"], w)
            self.assertEqual(dl["accumulated-weight-g"], acc_w)
            # next imbricated list
            self.assertTrue("pilars" in dl)
            self.assertEqual(len(dl["pilars"]), 4)
            for j in range(4):
                ds = dl["pilars"][j]
                self.assertTrue("diam-mm" in ds)
                self.assertEqual(ds["diam-mm"], acc_w / 100)


# FFS
# https://medium.com/@vladbezden/using-python-unittest-in-ipython-or-jupyter-732448724e31
unittest.main(argv=["first-arg-is-ignored"], exit=False)
