#!/usr/bin/env python

from math import pi

from pita.walker import Pita, walker_log

# Compared to the previous example, Add the height
stub = {
    "circles": [
        {"radius": 4, "height": 4},
        {"radius": 6, "height": 5},
    ]
}


p = Pita(stub, auto_hide=False)


@p.add_job("/circles/*", "radius", "area")
def circle_area(radius):
    return pi * radius**2


# area is computed by the previous function, height is given in the stub
@p.add_job("/circles/*", ["area", "height"], "volume")
def cylinder_volume(area, height):
    return area * height


status = p.run()

with open("example_1.json", "w+") as f:
    p.json_dump_root(walker_log.dot, file=f)
