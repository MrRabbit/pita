#!/usr/bin/env python3

import unittest

from pita.chooser import Chooser, Param, Arg


class Choose(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_a(self):
        """
        candidates: [[1, 1], [2, 1], [1, 2], [2, 2]]
        scores: [-1, 0, -3, -2]
        """

        v = Chooser(
            [
                Param("a", range(1, 3), lambda x: x),
                Param("b", range(1, 3), lambda x: -2 * x),
            ]
        )
        self.assertEqual(v.values, [2, 1])

    def test_b(self):
        """
        candidates: [
            [2, 1, 6], [3, 1, 7], [4, 1, 8],
            [2, 1.7, 6.7], [3, 1.7, 7.7], [4, 1.7, 8.7],
            [2, 3.4, 8.4], [3, 3.4, 9.4], [4, 3.4, 10.4]]
        scores: [
            -4.589999999999997, -0.9399999999999984, -inf (rejected),
            -2.583333333333331, -0.3333333333333325, -0.08333333333333337,
            -1.7900000000000005, -2.9400000000000026, -6.090000000000004
        ]
        """

        def compute_c(x, a, b):
            return x + a + b

        v = Chooser(
            [
                # range input. Weighting range:  [0:1]
                Param("a", range(2, 5), lambda x: (x - 1) / 4),
                # list input. Weighting range: [-2:0]
                Param("b", [1, 1.7, 3.4], lambda x: (x - 1) / -1.2),
                Param(
                    "c",
                    # accepted values: any but 8
                    lambda x: x != 8,
                    # weighting function
                    lambda x: -((x - 8.2) ** 2),

                    compute_c,
                    # Use 3, the result of a and b as the inpute of compute_c
                    3,
                    Arg("a"),
                    Arg("b"),
                ),
            ]
        )
        self.assertEqual(v.values, [4, 1.7, 8.7])


# FFS
# https://medium.com/@vladbezden/using-python-unittest-in-ipython-or-jupyter-732448724e31
unittest.main(argv=["first-arg-is-ignored"], exit=False)
